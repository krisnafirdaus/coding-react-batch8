import React, { useState, useEffect} from 'react'
import Axios from 'axios'
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'
import Home from './pages/Home/Home'
import About from './pages/About'
import Detail from './pages/Detail'
import Example from './pages/Example'
import User from './pages/User'
import View from './pages/View'
import List from './pages/List'
import Navbar from './components/Navbar'
import Footer from './components/Footer'
import './App.css';

function App() {
	let token = null
	const [data, setData] = useState([])


	useEffect(() => {
		Axios.get('https://jsonplaceholder.typicode.com/todos/1').then((res) => setData(res.data))
	}, [])

	
  return (
    <Router>
			<Navbar handleModal={() => {}} />

			<Switch>
				<Route exact path="/">
					{token ? <Redirect to="/detail" /> :	<Home/>}
				</Route>
				<Route exact path="/about">
						<About/>
				</Route>
				<Route exact path="/detail">
						<Detail/>
				</Route>
				<Route exact path="/detail/:id">
						<Detail/>
				</Route>
				<Route exact path="/example">
						<Example/>
				</Route>
				<Route exact path="/user">
						<User/>
				</Route>
				<Route exact path="/view/:id">
						<View/>
				</Route>
				<Route exact path="/list">
						<List/>
				</Route>
			</Switch>
			<Footer />
		</Router>
  );
}

export default App;

// <div className="App">
//   {/* <header className="App-header">
	
// 		<h1>Krisna Firdaus</h1>
// 		<h2>Andi Muhammad</h2>

		
		
//   </header>

// 	<div className='btn-all'>
// 		<button>Reset</button>
// 		<button>Stop</button>
// 		<button>Start</button>
// 	</div> */}
// 	{/* <Home dataProps={data} /> */}
// 	<Example />

// </div>