import React from 'react'
import {Box, Typography, Button} from '@mui/material'

const Content = ({item, setEditModal, setEditData}) => {

	return (
		<Box sx={{
			border: '1px solid black',
			display: 'flex',
			justifyContent: 'space-between',
			alignItems: 'center',
			paddingX: '10px',
			paddingY: '10px'
		}}>
			<Box>
				<Typography variant="h6" component="div" sx={{ mx: 'auto'}}>
					{item.name}
				</Typography>
				<Typography variant="h6" component="div" sx={{ mx: 'auto'}}>
					{item.address}
				</Typography>
			</Box>
			<Box>
				<Typography variant="h6" component="div" sx={{ mx: 'auto'}}>
					{item.hobby}
				</Typography>
				<Button sx={{
						background: '#00FFFF',
						color: '#ffffff',
						fontWeight: 600,
					}} variant='contained' color="inherit"
						onClick={() => {
							setEditModal(true)
							setEditData(item)
						}}
					>
						Edit
				</Button>
			</Box>
		</Box>
	)
}

export default Content