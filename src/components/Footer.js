import React from 'react'
import {Box, Typography} from '@mui/material'
import {LocalPhone, Instagram, YouTube, Telegram, Email} from '@mui/icons-material'

const Footer = () => {
	const data = [
		{
			id: 1,
			icon: <LocalPhone />
		},
		{
			id: 2,
			icon: <Instagram />
		},
		{
			id: 3, 
			icon: <YouTube />
		},
		{
			id: 4, 
			icon: <Telegram />
		},
		{
			id: 4, 
			icon: <Email />
		},
	]

	return (
		<Box sx={{
			backgroundColor: '#5B4947',
			marginTop: '148px',
			display: 'flex',
		}}>
			<Box
				sx={{
					paddingY: '24px',
					paddingX: '95px'
				}}
			>
				<Box
					sx={{
						marginBottom: '16px'
					}}
				>
					<Typography sx={{ color: '#FABC1D', fontWeight: 500, fontSize: '16px', marginBottom: '10px'}}>
						Address
					</Typography>
					<Typography sx={{ color: '#FFFFFF', fontWeight: 400, fontSize: '14px', width: '350px'}}>
						Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.
					</Typography>
				</Box>
				<Box>
					<Typography sx={{ color: '#FABC1D', fontWeight: 500, fontSize: '16px', marginBottom: '10px'}}>
						Contact Us
					</Typography>
					<Box
						sx={{ display: 'flex',  }}
					>
						{data.map(item => {
							return (
								<Box 	key={item.id} sx={{
									backgroundColor: '#FABC1D',
									borderRadius: '100%',
									marginRight: '16px'
								}}>
									<Box sx={{ padding: "6px", display: 'flex', alignItems: 'center'}}>
										{item.icon}
									</Box>
								</Box>
							)
						})}
					</Box>
				</Box>
			</Box>
		</Box>
	)
}

export default Footer