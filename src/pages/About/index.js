import React, {useEffect, useState} from 'react'
import Axios from 'axios'

const About = () => {
	const apiUrl = 'https://jsonplaceholder.typicode.com'
	const [data, setData] = useState({
		title: '',
		body: '',
		userId: 1
	})

	const callApi = () => {
		Axios.get(`${apiUrl}/posts`, {}, {
			// headers: {
			// 	'Content-Type': 'application/json',
			// 	Authorization: `Bearer ${token}`
			// }
		}).then((res) =>	setData(res.data))
	}

	useEffect(() => {
		callApi()
	}, [])

	const handleSubmit = () => {
		Axios.post(`${apiUrl}/posts`, {
			...data
			}).then((res) => {
				console.log(res)
			})
	}

	const handleInput = (e) => {
		const event = e.target
		const name = event.name
		const value = event.value

		setData({
			...data,
			[name]: value
		})
	}


	return (
		<div>
			{/* {data.map((item) => {
				return (
					<div>
						<h1>{item.part_name}</h1>
						<h2>{item.spesifikasi}</h2>
					</div>
				)
			})} */}
			<h1>About</h1>

			<label>Title</label>
			<input name="title" onChange={handleInput} />

			<label>Body</label>
			<input name="body" onChange={handleInput} />

			<button onClick={handleSubmit}>Submit</button>
		</div>
	)
}

export default About