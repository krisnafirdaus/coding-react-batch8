import React, {useEffect, useState} from 'react'
import Axios from 'axios'
import {useParams} from 'react-router-dom'

const Detail = () => {
	const {id} = useParams()
	const [data, setData] = useState({})

	useEffect(() => {
		if(id){
			Axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`).then((res) => setData(res.data))
		}
	}, [])


	return (
		<div>
			<h1>Detail</h1>
			<h1>{id}</h1>
			<h1>{data.title}</h1>
			<h2>{data.body}</h2>
		</div>
	)
}

export default Detail