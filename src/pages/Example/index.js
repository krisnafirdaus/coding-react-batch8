import React, {useState} from 'react'

const Example = () => {
	// const [list, setList] = useState(['krisna', 'andi', 'anto'])
	const [list, setList] = useState(
		[
			{
				id: 1,
				name: 'krisna'
			},
			{
				id: 2,
				name: 'andi'
			}
		]
	)
	const [name, setName] = useState('')


	return (
		<div>

			<div>Example</div>
			<ul>
				{list.map((item) => {
					return (
						<li key={item.id}>{item.name}</li>
					)
				})}
			</ul>
			<div>
			<label>Name</label>
				<input 
					type='text'
					maxLength="10"
					disabled={name.length > 10}
					onChange={(e) => {
						setName(e.target.value)

						if(name.length > 4){
							alert(`Nama masukkan sudah lebih dari ${name.length}`)
						}
					}}
				/>
			</div>
		</div>
	)
}

export default Example