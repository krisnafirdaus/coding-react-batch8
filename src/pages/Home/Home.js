import React, { useState, useEffect } from 'react'
import {Link, useLocation} from 'react-router-dom'
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

import './Home.css'


const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const Home = ({dataProps}) => {
	let location = useLocation()
	const [data, setData] = useState([])
	// const [name, setName] = useState('andi')

	// console.log(dataProps);



	return (
		<div className='home-page'>

			<h1>Home</h1>
			<Link
				to="/detail"
			>
				Detail
			</Link>
			<Button variant="text">Text</Button>
			<Button variant="contained">Contained</Button>
			<Button variant="outlined">Outlined</Button>

			<Card sx={{ minWidth: 275 }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          Word of the Day
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          adjective
        </Typography>
        <Typography variant="body2">
          well meaning and kindly.
          <br />
          {'"a benevolent smile"'}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Learn More</Button>
      </CardActions>
    </Card>

		<Stack direction="column" spacing={2}>
			<Item>Item 1</Item>
			<Item>Item 2</Item>
			<Item>Item 3</Item>
		</Stack>
			{/* <a href="/detail">
				Detail
			</a> */}
			{/* <h2>{name}</h2>
			<button onClick={() => setName('krisna')}>Klik Ubah</button> */}
		</div>
	)
}

export default Home