import { Box, Grid, Container, Card, CardContent, Typography, CardActions, Button } from '@mui/material'
import React from 'react'

const List = () => {
	const dataAnggota = [
		{
			id: 1,
			image: 'https://images.unsplash.com/photo-1687721449660-f902b9d22624?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=774&q=80',
			name: 'Krisna Firdaus',
			address: 'Indonesia'
		},
		{
			id: 2,
			image: 'https://images.unsplash.com/photo-1687721449660-f902b9d22624?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=774&q=80',
			name: 'Andi Firdaus',
			address: 'Argentina'
		}
	]

	return (
		<Container>
			<Grid container sx={{ marginTop: '24px'}}>
				<Grid xs={12} md={8}>
					<Box sx={{ border: '1px solid red', height: '50vh', marginRight: '20px'}}>
						<Grid container>
							<Grid xs={6}>
							<Card sx={{ minWidth: 275, marginX: '20px', marginTop: '20px' }}>
							<CardContent>
								<Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
									Word of the Day
								</Typography>
								<Typography sx={{ mb: 1.5 }} color="text.secondary">
									adjective
								</Typography>
								<Typography variant="body2">
									well meaning and kindly.
									<br />
									{'"a benevolent smile"'}
								</Typography>
							</CardContent>
							<CardActions>
								<Button size="small">Learn More</Button>
							</CardActions>
						</Card>
							</Grid>
							<Grid xs={6}>
							<Card sx={{ minWidth: 275, marginX: '20px', marginTop: '20px' }}>
							<CardContent>
								<Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
									Word of the Day
								</Typography>
								<Typography sx={{ mb: 1.5 }} color="text.secondary">
									adjective
								</Typography>
								<Typography variant="body2">
									well meaning and kindly.
									<br />
									{'"a benevolent smile"'}
								</Typography>
							</CardContent>
							<CardActions>
								<Button size="small">Learn More</Button>
							</CardActions>
						</Card>
							</Grid>
						</Grid>
						
					</Box>
        </Grid>
        <Grid xs={12} md={4}>
					<Box sx={{ border: '1px solid blue', height: '50vh'}}>
						<Box sx={{ padding: '20px' }}>
							{dataAnggota.map((item, index) => {
								return (
									<Box sx={{ paddingTop: index !== 0 ? '10px' : '' }}>
										<Grid container>
											<Grid xs={3}>
												<img src={item.image} alt="image" height={60} width={60} style={{ borderRadius: '100%'}} />
											</Grid>
											<Grid xs={8}>
												<Typography color={'tomato'} fontWeight={500} fontSize={'20px'}>
													{item.name}
												</Typography>
												<Typography>
													{item.address}
												</Typography>
											</Grid>
										</Grid>
									</Box>
								)
							}
							)}
						</Box>
					</Box>
        </Grid>
			</Grid>
		</Container>
	)
}

export default List