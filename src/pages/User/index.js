import React, {useState} from 'react'
import Navbar from '../../components/Navbar'
import Content from '../../components/Content'
import Empty from '../../components/Empty'
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { Container, TextField } from '@mui/material';


// styled component

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};


const User = () => {
	const [open, setOpen] = useState(false)
	const [editModal, setEditModal] = useState(false)
	const [editData, setEditData] = useState({})
	const [search, setSearch] = useState('')


	const data = [
		{
			id: 1,
			name: 'krisna',
			hobby: 'coffee',
			address: 'indonesia'
		},
		{
			id: 2,
			name: 'andi',
			hobby: 'reading',
			address: 'argentina'
		}
	]

	const handleClose = () => {
		setOpen(false)
	}

	const handleCloseModal = () => {
		setEditModal(false)
	}

	const handleOpenModal = () => {
		setOpen(true)
	}

	const filterData = data.filter((data) => {
		return data.name.includes(search) || data.hobby.includes(search) || data.address.includes(search)
	})


	return (
		<Container>

			<Box sx={{
				marginY: '20px'
			}}>
				<TextField
					id="outlined-controlled"
					label="Controlled"
					sx={{
						width: '50%'
					}}
					value={search}
					onChange={(event) => {
						setSearch(event.target.value);
					}}
				/>
			</Box>
			{ data.length > 0 ? filterData?.map((item) => {
				return (
					<Content item={item} setEditModal={setEditModal} setEditData={setEditData} />
				)
			}) : <Empty /> }
			<Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="parent-modal-title"
				aria-describedby="parent-modal-description"
			>
				<Box sx={{ ...style, width: 400 }}>
					<h2 style={{ color: 'red' }} id="parent-modal-title">Text in a modal</h2>
					<p id="parent-modal-description">
						Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
					</p>
				</Box>
			</Modal>
			<Modal
				open={editModal}
				onClose={handleCloseModal}
				aria-labelledby="parent-modal-title"
				aria-describedby="parent-modal-description"
			>
				<Box sx={{ ...style, width: 400 }}>
					<input value={editData.name} />
					<p>{editData.name}</p>
					<p>{editData.hobby}</p>
				</Box>
			</Modal>
		</Container>
	)
}

export default User