import { Box } from '@mui/material'
import React from 'react'
import {useParams} from 'react-router-dom'

const View = () => {
	const {id} = useParams()

	const data = [
		{
			id: 1,
			name: 'John Doe',
			hobby: 'Coding',
			address: 'Indonesia'
		},
		{
			id: 2,
			name: 'Messi',
			hobby: 'Football',
			address: 'Argentina'
		}
	]

	const detail = data.find((item) => item.id === parseInt(id))


	return (
		<Box sx={{ display: 'flex', flexDirection: 'column', textAlign: 'center' }}>
			<h1>{detail?.name}</h1>
			<h2>{detail?.hobby}</h2>
			<h3>{detail?.address}</h3>
		</Box>
	)
}

export default View